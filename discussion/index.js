// console.log("Hello S19");


// IF STATEMENT
/*
	executes a statement if a specifiec condition is true
	can stand alone without the else statement

	Syntax:
		if(condition){
			statement/code block
		}
*/

let numA=-1;
if(numA < 0){
	console.log("Hello");
}

console.log(numA<0);

if(numA>0){
	console.log("This statement will not be printed!");
};

let city="New York";

if(city==="New York"){
	console.log("Welcome to New York city!");
} 
// ELSE IF CLAUSE
/*
	executes a statement if previous conditions are false and if the specified condition is true
	"else if" clause is optional, can be added to capture additional conditions to change the flow of a program


*/

let numB=1;
if(numA>0){
	console.log("Hello!");
} else if (numB>0){
	console.log("World");
}

if(numA<0){
	console.log("Hello!");
} else if (numB>0){
	console.log("World");
}


city ="Tokyo";
if(city==="New York"){
	console.log("Welcome to New York city!");
} else if(city === "Tokyo"){
	console.log("Welcome to Tokyo");
}

// ELSE STATEMENT
/*
	executes a statement if all other conditions are false
	the "else"statement is optional and can be added to capture any other result to change the flow of a program
*/

if(numA>0){
	console.log("Hello!");
} else if (numB===0){
	console.log("World");
} else{
	console.log("Again");
}

/*let age=prompt("Enter your age");
if (age<=18){
	console.log("Not allowed to drink!");
} else if (age>=75) {
	console.log("Go Rest In Peace!");
} else{
	console.log("Shot na");
}*/

/*let height=prompt("Enter your height in cm");
if (height<150){
	console.log("Did not passed the minimum height requirement")
} else {
	console.log("Passed the minimum height requirement");
}*/

/*function qualitification(myHeight){
	if (myHeight<150){
		console.log("Did not passed the minimum height requirement")
	} else {
	console.log("Passed the minimum height requirement");
	}
}
qualitification(myHeight=prompt("enter your height in cm"));*/


let message ="no message";
console.log(message);

function determineTyphoonIntensity(windSpeed){

	if (windSpeed<30){
		return 'not a typhoon yet'
	} else if (windSpeed <= 61){
		return 'Tropical Depression detected'
	} else if (windSpeed >= 62 && windSpeed <=88){
		return 'Tropical Storm detected'
	} else if (windSpeed >=89 && windSpeed <= 117){
		return 'Severe tropical storm detected'
	} else {
		return 'Typhoon detected'
	}

}

message=determineTyphoonIntensity(69);
console.log(message);

if (message=="Tropical Storm detected"){
	console.warn(message);
}

// TRUTHY and FALSY
/*
	In Javascript, a truthy value is a value that is considered true when encountered in a boolean context
	values are considered tru unless defined otherwise

	Falsy values/exceptions for truthy
		false
		0
		-0
		""
		null
		undefined
		NaN

*/

if (true){
	console.log("Truthy!")
}

if(1){
	console.log("Truthy!!")
}
if([]){
	console.log("Truthy!!!")
}

if (false){
	console.log("Falsy!");
}
if (0){
	console.log("Falsy!!");
}

if (-0){
	console.log("Falsy!!!");
}

if (undefined){
	console.log("Falsy!!!!");
}

// CONDITIONAL (TERNARY) OPERATORS - shorthand
/*
	The ternary operator takes in three operands
	1. condition
	2. expression to execute if the condition is truthy
	3. expression to execute if the condition is falsy
		
		SYNTAX
			(condition) ? ifTrue : ifFalse
*/

// Single statement execution

let ternaryResult=(1<18) ? "Totoo" : "Hindi";
console.log("Result of ternary operator: "+ternaryResult);


let name;

function isOfLegalAge(){
	name = "John";
	return 'You are of legal age limit';
}

function isUnderAge(){
	name = 'Jane';
	return 'You are under the age limit'
}

/*let age=parseInt(prompt("what is your age"));
console.log(age);
console.log(typeof age);
let legalAge=(age>=18) ? isOfLegalAge() : isUnderAge() ;
console.log("Result of ternary operator in function: "+legalAge+", "+name);*/

// SWITCH statement
/*
	SYNTAX
		switch (expression){
			case value :
				statement
				break;
			default :
				statement;
				break
		}
*/

/*let day=prompt("What day of the week is it today?") .toLowerCase();
console.log(day);

	switch (day){
		case 'monday' :
			console.log("The color of the day is red");
			break;
		case 'tuesday' :
			console.log("The color of the day is orage");
			break;
		case 'wednesday' :
			console.log("The color of the day is yello");
			break;
		case 'thursday' :
			console.log("The color of the day is green");
			break;
		case 'friday' :
			console.log("The color of the day is pink");
			break;
		case 'saturday' , 'sabado' :
			console.log("The color of the day is blue");
			break;
		case 'sunday' , 'lingo' :
			console.log("The color of the day is indigo");
			break;
		default: 
			console.log("Please input a valid day!");
			break;
	}*/


// TRY-CATCH-FINALLY statement

/*
	try {
 		tryCode - Code block to run
	}
	
	catch(err) {
  		catchCode - Code block to handle errors
	}
	
	finally {
  			finallyCode - Code block to be executed regardless of the try result
}
*/

function showIntensityAlert(windSpeed){
	try{
		//attempts to execute a code
		alerat(determineTyphoonIntensity(windSpeed))
	}

	catch (error) {
		// catch errors within "try" statement
		console.log(error);
		console.warn(error.message);
	}

	finally {
		// continue execution of code regarless o success or failure of code execution in the try block to handle/resolve errors.
		alert("Intensity updates will show new alert.")
	}

}	

showIntensityAlert(56);
